<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  //header("location: login.html");
  header("location: http://localhost:80/login.html");
  exit;
}
?>



<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./styles/styles.css">
  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }
    .capa {
          border: 3px solid red;
          background-color: whitesmoke;
          position: absolute;
          width: 500px;
          height: 500px;
          top: 100px;
          left: 70px;
      } 
      .capa2 {
            border: 3px solid green;
            position: absolute;
            width: 300px;
            height: 300px;
            top: 100px;
            left: 600px;
        }
  </style>
  <script type="text/javascript" src="js/llamadasBasesDeDatos.js"></script>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: '#mytextarea'
    });
  </script>
  <script>
    var visible_actual=false;
    function visualiza(){
      if(visible_actual){
        document.getElementById("crearPresentacionDiv").style.display='none';
        
      }
      visible_actual=true;
      document.getElementById("crearPresentacionDiv").style.display='block';
      
  
    }
  </script>

  

</head>

<body onload="buscarPresentacionPropia()">

  <div class="grid-container">
    <div class="item1">
      <div class="logo">
        <a href="/Inicio.php">
          <img src="logo.png" alt="HTML tutorial" style="width:300px;height:200px;">
        </a>
      </div>
      <div>
        <h3>
          <?php
          if ($_SESSION["loggedin"] == false) {
            echo "Log in  ";
          }
          echo $_SESSION['username']; ?>
        </h3>
        <a href="login/logout.php" class="button">Deslogearse</a>
      </div>


    </div>
    <div class="item2">
      <div id="BuscarPresentacion">
        <form class="BuscarForm" action="javascript:buscarPresentacionPropia()" method="POST">
          <hr>
          <label for="id_user">ID del usuario</label>
          <input id="id_user" name="id_user" type="text" value="<?php echo $_SESSION['username']; ?>" disabled>
          <hr>
        </form>
      </div>


    </div>
    <div class="item3">
      <div>
        <div id="crearPresenta">
          
          <button class="button"  onclick=visualiza()> Crear Presentacion</button>
         
          <div id = "crearPresentacionDiv">

            <!--<input type="nombreUserInput" value ="<?php  echo $_SESSION['username']; ?>" disabled>-->
            ID_Usuario: <input id ="idUsuario" type="input" value= "<?php  echo $_SESSION['id']; ?>" disabled><br>
            Nombre de Presentacion : <input id="nombrePresentacion" type ="input" value=""><br>
            Privacidad: <input id = "publico" type="checkbox" name="OPT" value="true" ><br>

            <button class="button"  onclick= "crearPresentacion()"> Guardar Presentacion</button>

          </div>
        </div>
        <div>
        <a href="agregarLamina.php" class="button">Agregar Lamina</a>
        </div>

        <div id="LeerLamina">

          <form class="LoginForm" action="javascript:leerLamina()" method="POST">
            <hr>
            <label for="numeroDeLamina">numeroDeLamina</label>
            <input id="numeroDeLamina" name="numeroDeLamina" type="text" required>
            <label for="nombreDeLamina">nombreDeLamina</label>
            <input id="nombreDeLamina" name="nombreDeLamina" type="text">
            <label>Tamaño:</label>
            <select id="tamano" name="tamano">
              <option value="800X600">800X600</option>
              <option value="850X650">850X650</option>
              <option value="900X700">900X700</option>
              <option value="1000X800">1000X800</option>
              <option value="1020X850">1020X850</option>
            </select>
            <select id="transicion" name="transicion">
              <option value="1">Estilo 1</option>
              <option value="2">Estilo 2</option>
              <option value="3">Estilo 3</option>
              <option value="4">Estilo 4</option>
              <option value="5">Estilo 5</option>

            </select>
            <label>Tipo de color</label>
            <select onclick="deshabilitarColor2()" id="tipoDeColor">
              <option>Color Plano</option>
              <option>Transicion</option>
            </select>
            <label>Color1:</label>
            <input id="color1" name="color1" type="color" value="#ff0000">
            <label>Color2:</label>
            <input id="color2" name="color2" type="color" value="#ff0000">
            <label>Tipo de borde</label>
            <select id="tipoDeBorde">
              <option value="liso">liso</option>
              <option value="grueso">grueso</option>
            </select>


            <input class="button" type="submit" value="Leer Lamina">
            <div id="loginspinner" class="loader">Loading...</div>
            <hr>
          </form>
        </div>
        <h1>Que contiene la lamina</h1>
        <textarea id="mytextarea"></textarea>

        <button class="button" onclick="ModificarLamina()">ModificarLamina</button>
      </div>

      <h1>Mis Presentaciones</h1>
      <div>

        <div id="Presentacion"></div>
      </div>

    </div>

  </div>
  <script>
    
  </script>

</body>

</html>