var datosUsuario;
var localStorage = window.localStorage;

/**
 * Funcion para inicializar el localstorage en la pagina
 */
function initLocalStorage() {
    window.datosUsuario = JSON.parse(localStorage.getItem("datosUsuario")) || new Object();
}

/**
 * Agregamos un listener para que siempre cargue apenas cargue la pagina
 */
document.addEventListener("DOMContentLoaded", initLocalStorage());

/**
 * Grabamos lo que esta en datosUsuarios al localstorage para que persiste
 */
function writeLocalStorage() {
    localStorage.setItem("datosUsuario", JSON.stringify(datosUsuario));
}

function aplicarCSS(div, props) {
    div.style.width = props.width
    div.style.heigth = props.heigth
    if (props.tipoDeColor === 'Transicion') {
        div.style.backgroundImage = 'linear-gradient(' + props.color1 + ', ' + props.color2 + ')';
    } else
        div.style.backgroundColor = props.color1;
    div.className = props.borde
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// LAMINAS ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function agregarLamina() {
    var xhttp = new XMLHttpRequest();
    var respuesta;
    document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!this.responseText.includes("error")) {
                respuesta = this.responseText;
                console.log(respuesta);
                console.log("Se agrego la lamina");
                window.alert(respuesta);

            }
        };

    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = "agregarLamina";
    //Aqui se ponen tods los parametros
    idPresentacion = document.getElementById("idPresentacion").value;
    nombreLamina = document.getElementById("nombreDeLamina").value;

    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = 'agregarLamina';
    //Aqui se ponen tods los parametros
    idPresentacion = document.getElementById("idPresentacion").value;
    nombreLamina = document.getElementById("nombreDeLamina").value;

    detalles = {
        "tamano": document.getElementById("tamano").value,
        "tipoDeColor": document.getElementById("tipoDeColor").value,
        "color1": document.getElementById("color1").value,
        "color2": document.getElementById("color2").value,
        "contenido": tinymce.activeEditor.getContent(),
        "transicion": document.getElementById("transicion").value,
        "borde": document.getElementById("tipoDeBorde").value
    }
    detalles = JSON.stringify(detalles);
    //Aqui se funden los parametros en una sola linea para ser enviado
    params =
        "tipoDeLlamada=" +
        tipoDeLlamada +
        "&idPresentacion=" +
        idPresentacion +
        "&nombreLamina=" +
        nombreLamina +
        "&detalles=" +
        detalles;

    xhttp.send(params);
}

function modificarLamina() {
    var xhttp = new XMLHttpRequest();
    var respuesta;
    document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!this.responseText.includes("error")) {
                respuesta = this.responseText;
                console.log(respuesta);
                console.log("Se agrego la lamina");
                window.alert(respuesta);

            }
        };

    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos

    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = 'modificarLamina';
    //Aqui se ponen tods los parametros
    idPresentacion = document.getElementById("idPresentacion").value;
    idLamina = document.getElementById("numeroDeLamina").value;
    nombreLamina = document.getElementById("nombreDeLamina").value;

    detalles = {
        "tamano": document.getElementById("tamano").value,
        "tipoDeColor": document.getElementById("tipoDeColor").value,
        "color1": document.getElementById("color1").value,
        "color2": document.getElementById("color2").value,
        "contenido": tinymce.activeEditor.getContent(),
        "transicion": document.getElementById("transicion").value,
        "borde": document.getElementById("tipoDeBorde").value
    }
    detalles = JSON.stringify(detalles);
    //Aqui se funden los parametros en una sola linea para ser enviado
    params =
        "tipoDeLlamada=" +
        tipoDeLlamada +
        "&idLamina=" +
        idLamina +
        "&idPresentacion=" +
        idPresentacion +
        "&nombreLamina=" +
        nombreLamina +
        "&detalles=" +
        detalles;

    xhttp.send(params);
}

function leerLamina() {

    var xhttp = new XMLHttpRequest();
    var respuesta;
    document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!(this.responseText == "No hay resultados")) {
                respuesta = JSON.parse(this.responseText);
                console.log(respuesta);
                if (respuesta.id) {
                    console.log("Se Encontro la lamina: ");
                    console.log(respuesta);
                    document.getElementById("nombreDeLamina").value = respuesta.nombre;
                    tinymce.activeEditor.setContent(
                        JSON.parse(respuesta.detalles).contenido
                    );
                    detalle = JSON.parse(respuesta.detalles)
                    document.getElementById("tamano").value = detalle.tamano
                    document.getElementById("tipoDeColor").value = detalle.tipoDeColor
                    document.getElementById("color1").value = detalle.color1
                    document.getElementById("color2").value = detalle.color2
                    document.getElementById("transicion").value = detalle.transicion
                    document.getElementById("tipoDeBorde").value = detalle.borde
                }
            } else {
                window.alert("No existe el id de la lamina");
            }
            document.getElementById("loginspinner").style.display = "none";
            //revision de que el primer resultado tenga el campo id
        }
    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = "LeerLamina";
    //Aqui se ponen tods los parametros
    idLamina = document.getElementById("numeroDeLamina").value;

    //Aqui se funden los parametros en una sola linea para ser enviado
    params = "tipoDeLlamada=" + tipoDeLlamada + "&idLamina=" + idLamina;

    xhttp.send(params);
}


function leerLaminasDePresentacion() {
    var xhttp = new XMLHttpRequest();
    document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!(this.responseText == 'No hay resultados')) {
                var response = JSON.parse(this.responseText);
                guardarDOM(response);
                mostrarCapas();
            } else {
                window.alert("No se encontraron láminas para la presentación.");
            }
            document.getElementById("loginspinner").style.display = "none";
            //revision de que el primer resultado tenga el campo id
        }
    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = 'laminasDePresentacion';
    //Aqui se ponen tods los parametros
    idPresentacion = document.getElementById("idpresentation").value;
    //validacion extra para el evento onload
    myStorage = window.localStorage;
    const urlParams = new URLSearchParams(window.location.search);
    if (!(urlParams.get('idPresentacion') === "") && urlParams.has('idPresentacion')) {
        idPresentacion = urlParams.get('idPresentacion');
        document.getElementById("idpresentation").value = urlParams.get('idPresentacion');
    }
    //Aqui se funden los parametros en una sola linea para ser enviado
    params = 'tipoDeLlamada=' + tipoDeLlamada + '&idPresentacion=' + idPresentacion;
    xhttp.send(params);
}


var listaCapas = [];
var actual = 0;




function guardarDOM(response) {
    for (var i in response) {
        var nombre = response[i].nombre;
        var numDiapositiva = response[i].orden;
        var detalles = JSON.parse(response[i].detalles); /* { "tamano": "", "contenido": "..." } */
        var tamano = detalles.tamano.split("x"); /* height width */
        if (tamano.length = 1) {
            var tamano = detalles.tamano.split("X"); /* height width */
        }

        agregarElemento(tamano[0], tamano[1], detalles.contenido, nombre, numDiapositiva, detalles.borde, detalles);
    }
}

function agregarElemento(alto, ancho, contenido, nombre, numDiapositiva, borde, detalles) {
    nuevaCapa = new Object();
    nuevaCapa.alto = alto;
    nuevaCapa.ancho = ancho;
    nuevaCapa.contenido = contenido;
    nuevaCapa.nombre = nombre;
    nuevaCapa.numDiapositiva = numDiapositiva;
    nuevaCapa.detalles = detalles;
    nuevaCapa.color1=detalles.color1;
    if ((detalles.tipoDeColor === 'Transicion')) {
        nuevaCapa.backgroundColor = 'linear-gradient(' + detalles.color1 + ', ' + detalles.color2 + ')';
    } else
        nuevaCapa.backgroundColor = 'linear-gradient(' + detalles.color1 + ', ' + detalles.color1 + ')';


    nuevaCapa.html = document.createElement('div');
    nuevaCapa.html.innerHTML = contenido;
    nuevaCapa.html.style.width = ancho + 'px';
    nuevaCapa.html.style.height = alto + 'px';
    nuevaCapa.html.style.backgroundImage = nuevaCapa.backgroundColor
    nuevaCapa.html.className = borde + ' lamina';

    listaCapas.push(nuevaCapa);
}

function mostrarCapas() {
    document.getElementById('laminasContainer').innerHTML = '';
    document.getElementById('laminasContainer').style.opacity = '1'
    document.getElementById('laminasContainer').style.width = 'auto'
    document.getElementById('laminasContainer').style.height = 'auto'
    document.getElementById('laminasContainer').style.backgroundColor = '#FFFFFF'
    document.getElementById('laminasContainer').style.backgroundImage ='linear-gradient( #FFFFFF, #FFFFFF)';
    if (!(localStorage.getItem(document.getElementById("idpresentation").value)===null)) {
        actual = localStorage.getItem(document.getElementById("idpresentation").value);
    }
    for (const i in listaCapas) {

        if (i == actual) {
            switch (listaCapas[i].detalles.transicion) {
                case '1':
                    listaCapas[i].html.style.opacity = 0.
                    document.getElementById('laminasContainer').style.opacity = 0.1;
                    setTimeout(() => {
                        listaCapas[i].html.style.opacity = 1
                        document.getElementById('laminasContainer').style.opacity = 1;
                    }, 500);
                    break;
                case '2':
                    listaCapas[i].html.style.width = '0px'
                    document.getElementById('laminasContainer').style.width = '0px';
                    setTimeout(() => {
                        listaCapas[i].html.style.width = listaCapas[i].ancho + 'px'
                        document.getElementById('laminasContainer').style.width = listaCapas[i].ancho + 'px';
                        console.log('ancho')
                    }, 500);
                    break;
                case '3':
                    listaCapas[i].html.style.height = '0px'
                    document.getElementById('laminasContainer').style.height = '0px';
                    setTimeout(() => {
                        listaCapas[i].html.style.height = listaCapas[i].alto + 'px'
                        document.getElementById('laminasContainer').style.height = listaCapas[i].alto + 'px';
                        console.log('alto')
                    }, 500);
                    break;
                case '4':
                    listaCapas[i].html.style.height = '0px'
                    listaCapas[i].html.style.width = '0px'
                    document.getElementById('laminasContainer').style.height = '0px';
                    document.getElementById('laminasContainer').style.width = '0px';
                    setTimeout(() => {
                        listaCapas[i].html.style.height = listaCapas[i].alto + 'px'
                        listaCapas[i].html.style.width = listaCapas[i].ancho + 'px'
                        document.getElementById('laminasContainer').style.height = listaCapas[i].alto + 'px';
                        document.getElementById('laminasContainer').style.width = listaCapas[i].ancho + 'px';
                    }, 500);
                    break;
                case '5':
                    listaCapas[i].html.style.backgroundImage = '';
                    listaCapas[i].html.style.backgroundColor = '#53a7ea';
                    document.getElementById('laminasContainer').style.backgroundColor = '#53a7ea';
                    setTimeout(() => {
                        listaCapas[i].html.style.backgroundImage = listaCapas[i].backgroundColor
                        listaCapas[i].html.style.backgroundColor = listaCapas[i].color1;
                        document.getElementById('laminasContainer').style.backgroundColor = listaCapas[i].backgroundColor;
                    }, 500);
                    break;

                default:
                    break;
            }

            var numDia = "Diapositiva #" + listaCapas[i].numDiapositiva;
            /* pongo el titulo y el num de diapositiva */
            document.getElementById("idTextP").innerText = listaCapas[i].nombre;
            document.getElementById("idNumDia").innerText = numDia;
            listaCapas[i].html.style.display = 'block';
        } else {
            listaCapas[i].html.style.display = 'none';
        }
        document.getElementById("laminasContainer").appendChild(listaCapas[i].html);
    }


}

function pasarLamina(opt) {

    if (opt === 'a') {
        if (actual === 0) return;
        else actual--;
    } else {
        if (actual === listaCapas.length - 1) return;
        else actual++;
    }
    localStorage.setItem(document.getElementById("idpresentation").value,actual)
    mostrarCapas();
    
}




///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// PRESENTACIONES ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////



function buscarPresentacion() {

    var xhttp = new XMLHttpRequest();
    var respuesta;
    //document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            respuesta = JSON.parse(this.responseText);
            let idPresentacion = document.getElementById("Presentacion"); // id del div donde tiene que agregarlo
            idPresentacion.innerHTML = ""
            respuesta.forEach((element) => {
                var linkPresentacion = document.createElement("A"); // Create a <a> element
                linkPresentacion.innerHTML = element.nombre; // Insert text
                linkPresentacion.href = "/Presentaciones.php?idPresentacion=" + element.id; // la ruta a la presentacion 

                linkPresentacion.classList.add("gridItem"); //add class gridItem
                idPresentacion.appendChild(linkPresentacion); // le agrega al div de presentacion el nuevo div o link 'a'

            });
        }
    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = "buscarPresentacion";
    //Aqui se ponen tods los parametros
    id_user = document.getElementById("id_user").value;
    //Aqui se funden los parametros en una sola linea para ser enviado
    params = "tipoDeLlamada=" + tipoDeLlamada + "&id_user=" + id_user;
    xhttp.send(params);
}

function buscarPresentacionPropia() {
    var xhttp = new XMLHttpRequest();
    var respuesta;
    //document.getElementById("loginspinner").style.display = "block";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            respuesta = JSON.parse(this.responseText);
            let idPresentacion = document.getElementById("Presentacion"); // id del div donde tiene que agregarlo
            idPresentacion.innerHTML = ""
            respuesta.forEach((element) => {

                var linkPresentacion = document.createElement("A"); // Create a <a> element
                linkPresentacion.innerHTML = element.nombre; // Insert text
                linkPresentacion.href = "/Presentaciones.php?idPresentacion=" + element.id; // la ruta a la presentacion 

                linkPresentacion.classList.add("gridItem"); //add class gridItem
                idPresentacion.appendChild(linkPresentacion); // le agrega al div de presentacion el nuevo div o link 'a'
            });
        }
    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = "buscarPresentacionPropia";
    //Aqui se ponen tods los parametros
    id_user = document.getElementById("id_user").value;
    //Aqui se funden los parametros en una sola linea para ser enviado
    params = "tipoDeLlamada=" + tipoDeLlamada + "&id_user=" + id_user;
    xhttp.send(params);
}


function crearPresentacion() {
    var xhttp = new XMLHttpRequest();
    var respuesta;

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!(this.responseText.includes('error'))) {
                respuesta = this.responseText;
                console.log(respuesta);
                console.log("Se agrego la presentacion");
                window.alert(respuesta);

                document.getElementById("nombre").value = '';
                tinymce.activeEditor.setContent('');


            } else {
                window.alert("No existe el id de la presentacion" + this.responseText);
            }
            document.getElementById("loginspinner").style.display = "none";
            //revision de que el primer resultado tenga el campo id

        }
    };

    xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
    tipoDeLlamada = 'crearPresentacion';
    //Aqui se ponen tods los parametros
    nombrePresentacion = document.getElementById("nombrePresentacion").value;
    idUsuario = document.getElementById("idUsuario").value;
    publico = document.getElementById("publico").checked;

    //Aqui se funden los parametros en una sola linea para ser enviado
    params = 'tipoDeLlamada=' + tipoDeLlamada +
        '&nombre=' + nombrePresentacion + '&id_usuario=' + idUsuario + '&publico=' + publico;

    xhttp.send(params);
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// LOGIN Y REGISTRO ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function validarAcceso() {
  var xhttp = new XMLHttpRequest();
  var respuesta;
  document.getElementById("loginspinner").style.display = "block";
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          respuesta = eval(this.responseText);
          if (respuesta[0].validated) {
              console.log("Usuario validado");
              document.getElementById("loginspinner").style.display = "none";
              window.location.href = "index.php";
          }
      }
  };

  xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
  tipoDeLlamada = "Login";

  user_name = document.getElementById("user_name").value;
  password = document.getElementById("password").value;

  params =
      "tipoDeLlamada=" +
      tipoDeLlamada +
      "&user_name=" +
      user_name +
      "&password=" +
      password;

  xhttp.send(params);

}

function registrarse() {
  var xhttp = new XMLHttpRequest();
  var respuesta;
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          respuesta = this.responseText;

          if (respuesta === 'true') {
              window.alert('Usuario registrado, usted sera redirigido al inicio')
              window.location.href = "index.php";
          } else {
              window.alert('error: ' + respuesta);
          }
      }
  };


  xhttp.open("POST", "funcionesBaseDeDatos/LlamadasABasesDeDatos.php", true);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  //Aqui se selecciona que tipo de llamada se hace, para que el php use la funcion que programamos
  tipoDeLlamada = "Registrarse";
  //estos son los parametros necesarios para poder registrar un usuario nuevo
  user_name = document.getElementById("user_name").value;
  password = document.getElementById("password").value;
  nombre = document.getElementById("nombre").value;

  params =
      "tipoDeLlamada=" +
      tipoDeLlamada +
      "&user_name=" +
      user_name +
      "&nombre=" +
      nombre +
      "&password=" +
      password;

  xhttp.send(params);

}