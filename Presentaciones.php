<?php
// Initialize the session
session_start();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Presentaciones</title>
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/presentations.css">
    <link rel="stylesheet" href="styles/lamina.css">
    <script type="text/javascript" src="js/llamadasBasesDeDatos.js"></script>

</head>

<body onload="leerLaminasDePresentacion()">

    <div class="center">
        <form action="javascript:leerLaminasDePresentacion()" method="POST">
            <label for="idp_name">ID de presentación</label><br>
            <input id="idpresentation" type="number" name="idp_name" min="0" value="89" required disabled>
            
        </form>
    </div>

    <div class="center">

        <h3 id="idTextP" class="title_animation">Título</h3>
        <h4 id="idNumDia">Diapositiva #</h4>

        <button id="buttonAnt" class="button2" onclick="pasarLamina('a')"> < </button>
        <button id="buttonSig" class="button2" onclick="pasarLamina('s')"> > </button>

        <!-- Aquí va el contenido de las laminas -->
        <div id="laminasContainer" class="p_container">

        </div>
        <br>

        <div id="loginspinner" class="loader">Loading...</div>
    <script>

    </script>
</body>

</html>