<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    //header("location: login.html");
    header("location: http://localhost:80/login.html");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="./styles/styles.css">
    <meta charset="UTF-8">
    <title>Index</title>
    <script type="text/javascript" src="js/llamadasBasesDeDatos.js"></script>
    <!-- Script para poder utilizar tinymce -->
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
    
    <style>
        body {
            font: 14px sans-serif;
            text-align: center;
        }
        
    </style>
</head>

<body>
    <h1 class="my-5">Hola, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Bienvenido al sitio</h1>
    <p>
        <a href="agregarLamina.php" class="button">Agregar Lamina</a>
        <a href="login/logout.php" class="button">Deslogearse</a>
    </p>
    <!-- Este div contiene lo necesario para hacer el leer lamina. -->
    <div id="LeerLamina">
        <form class="LoginForm" action="javascript:leerLamina()" method="POST">
            <hr>
            <label for="numeroDeLamina">numeroDeLamina</label>
            <input id="numeroDeLamina" name="numeroDeLamina" type="text" required>
            <label for="nombreDeLamina">nombreDeLamina</label>
            <input id="nombreDeLamina" name="nombreDeLamina" type="text">
            <input class="button" type="submit" value="Leer Lamina">
            <div id="loginspinner" class="loader">Loading...</div>
            <hr>
        </form>
    </div>
    <h3>Que contiene la lamina</h3>
    <textarea id="mytextarea"></textarea>
    
    <button class="button" onclick="ModificarLamina()">ModificarLamina</button>

    


</body>

</html>