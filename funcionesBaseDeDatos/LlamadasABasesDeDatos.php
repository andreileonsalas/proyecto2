<?php
session_start();
// Check if the user is logged in, if not then redirect him to login page
function revisarLogin()
{
  if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    //header("location: login.html");
    header("location: http://localhost:80/login.html");
    exit;
  }
}



//Busca el archivo db.php que contiene las variables para poder conectarse al servidor, y la conexion a el
include($_SERVER['DOCUMENT_ROOT'] . '/db.php');
$tipoDeLlamada = $_POST['tipoDeLlamada'];
switch ($tipoDeLlamada) {
  case 'LeerLamina':
    //Revisa primero si esta logeado
    revisarLogin();
    //Buscara la lamina, y retornara el primer valor como un json stringificado, con el [0] devolvemos solo la primer fila
    $idLamina = $_POST['idLamina'];

    $conn = db_connect();
    $query = "
          SELECT * FROM public.laminas
          WHERE id = '$idLamina'
          ORDER BY id ASC 
          ";
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    if (!pg_num_rows($result))
      die("No hay resultados");
    echo json_encode(pg_fetch_all($result)[0]);

    break;
  case 'laminasDePresentacion':
  
    //Buscara las laminas segun el id de la presentaicon, y retornara todos los valores
    $idPresentacion = $_POST['idPresentacion'];
    $conn = db_connect();
    $query = "
    SELECT *
    from presentaciones INNER JOIN presentaciones_laminas 
    ON presentaciones.id = presentaciones_laminas.id_presentacion
    INNER JOIN laminas
    ON id_lamina = laminas.id 
    where presentaciones.id = '$idPresentacion'
    ORDER BY orden
            ";
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    if (!pg_num_rows($result))
      die("No hay resultados");
    echo json_encode(pg_fetch_all($result));

    break;
  case 'agregarLamina':
    //Revisa primero si esta logeado
    revisarLogin();
    //Buscara la ultima lamina y se agrega el siguiente
    $nombreLamina = $_POST['nombreLamina'];
    $idPresentacion = $_POST['idPresentacion'];
    $detalles = $_POST['detalles'];
    $conn = db_connect();
    $query = "
          SELECT * FROM public.presentaciones_laminas
          WHERE id_presentacion = '$idPresentacion'
          ";
    $result = pg_query($conn, $query) or die("Ocurrió un error en encontrar el ultimo id de lamina\n");
    //pendiente validar que la presentacion exista
    //if (!pg_num_rows( $result ))
    //  die("No hay resultados");
    $orden = pg_num_rows($result);
    $orden = $orden + 1;
    //$detalles = json_decode($detalles);
    $query = "
          INSERT INTO public.laminas(
            nombre, detalles)
            VALUES ('$nombreLamina', '{$detalles}'::json)
            RETURNING id;
          ";

    $result = pg_query($conn, $query) or die("Ocurrió un error en la insercion de la lamina.\n");
    $siguienteLamina = pg_fetch_result($result, 0, 0);
    $query = "
            INSERT INTO public.presentaciones_laminas(
            id_presentacion, id_lamina, orden)
            VALUES ($idPresentacion, $siguienteLamina, $orden);
          ";
    $result = pg_query($conn, $query) or die("Ocurrió un error en la insercion de la lamina.\n");

    echo 'Lamina Insertada correctamente';

    break;
  case 'modificarLamina':
    //Revisa primero si esta logeado
    revisarLogin();
    //Buscara la lamina, y retornara el primer valor como un json stringificado, con el [0] devolvemos solo la primer fila
    $idLamina = $_POST['idLamina'];
    $nombreLamina = $_POST['nombreLamina'];
    $detalles = $_POST['detalles'];
    $conn = db_connect();
    $query = "
          UPDATE public.laminas SET
          detalles = '{$detalles}'::json , nombre = '$nombreLamina' WHERE
          id = $idLamina;
          ";
    echo $query;
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    echo json_encode(pg_fetch_all($result)[0]);

    break;

  case 'crearPresentacion':
    //Revisa primero si esta logeado
    revisarLogin();
    //Buscara la lamina, y retornara el primer valor como un json stringificado, con el [0] devolvemos solo la primer fila
    //$idPresentacion = $_POST['id'];
    $nombre = $_POST['nombre'];
    $idUsuario = $_POST['id_usuario'];
    $publico = $_POST['publico'];

    $conn = db_connect();
    $query = "INSERT INTO public.presentaciones(nombre, id_usuario, publico) 
      VALUES ('$nombre',$idUsuario,$publico) RETURNING id;";

    echo $query;
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    echo json_encode(pg_fetch_all($result)[0]);

    break;

  case 'Login':
    $user_name = $_POST['user_name'];
    $password = $_POST['password'];
    $conn = db_connect();
    $query = "select count(*)>0 as validated from usuarios where email='$user_name' and password='$password'";
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    $_SESSION["loggedin"] = true;
    $_SESSION["username"] = $user_name;

    $query = "Select id from usuarios where email = '$user_name' ;";
    $resultuser = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");

    //$query = "Select publico from usuarios where publico = '$user_name' ;" ;

    $_SESSION["id"] = (pg_fetch_result($resultuser, 0));
    echo (json_encode(pg_fetch_all($result)));
    break;
  case 'Registrarse':
    $user_name = $_POST['user_name'];
    $password = $_POST['password'];
    $nombre = $_POST['nombre'];
    $conn = db_connect();
    $query = "
    INSERT INTO public.usuarios(
      nombre, email,password)
      VALUES ('$nombre', '$user_name','$password')
      RETURNING id;
    ";
    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");
    echo 'true';
    //echo (json_encode(pg_fetch_all($result)));
    break;

  case 'modificarLamina':
    echo 'Se modifico lamina';
    break;
  case 'buscarLammina':
    echo 'Se busco lamina';
    break;

  case 'buscarPresentacion':
    $id_user = $_POST['id_user'];
    $conn = db_connect();
    $query = "select usuarios.id , usuarios.nombre ,    presentaciones.id , presentaciones.nombre from usuarios
    inner join    presentaciones on usuarios.id = presentaciones.id_usuario 
    where usuarios.id = '$id_user'  and presentaciones.publico = true order by usuarios.id; ";

    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");

    echo (json_encode(pg_fetch_all($result)));
    break;

  case 'buscarPresentacionPropia':
    $id_user = $_SESSION["id"];
    $conn = db_connect();
    $query = "select usuarios.id , usuarios.nombre ,    presentaciones.id , presentaciones.nombre from usuarios
      inner join    presentaciones on usuarios.id = presentaciones.id_usuario 
      where usuarios.id = '$id_user'  order by usuarios.id; ";

    $result = pg_query($conn, $query) or die("Ocurrió un error en la consulta.\n");

    echo (json_encode(pg_fetch_all($result)));
    break;

  default:
    echo 'No selecciono ninguna opcion correcta';
}
