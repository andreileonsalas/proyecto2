<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  //header("location: login.html");
  header("location: http://localhost:80/login.html");
  exit;
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="./styles/styles.css">
  <meta charset="UTF-8">
  <title>Index</title>
  <script type="text/javascript" src="js/llamadasBasesDeDatos.js"></script>
  <!-- Script para poder utilizar tinymce -->
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: '#mytextarea'
    });
    function deshabilitarColor2(){
    if (document.getElementById("tipoDeColor").value ==='Color Plano') {
      document.getElementById("color2").disabled = true
    }
    else{
      document.getElementById("color2").disabled = false
    }
  }
  </script>

  <style>
    body {
      font: 14px sans-serif;
      text-align: center;
    }

    ,
    .capa {
      border: 3px solid red;
      background-color: whitesmoke;
      position: absolute;
      width: 500px;
      height: 500px;
      top: 100px;
      left: 70px;
    }
  </style>

</head>

<body>
  <h1 class="my-5">Hola, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Bienvenido al sitio</h1>
  <p>
    <a href="login/logout.php" class="button">Deslogearse</a>
  </p>
  <!-- Este div contiene lo necesario para hacer el leer lamina. -->
  <div id="LeerLamina">
    <form class="LoginForm" action="javascript:agregarLamina()" method="POST">
      <hr>
      <label for="idPresentacion">idPresentacion</label>
      <input id="idPresentacion" name="idPresentacion" type="text" required disabled>
      <label for="nombreDeLamina">nombreDeLamina</label>
      <input id="nombreDeLamina" name="nombreDeLamina" type="text" required>
      <label>Tamaño:</label>
      <select id="tamano" name="tamano">
        <option value="800X600">800X600</option>
        <option value="850X650">850X650</option>
        <option value="900X700">900X700</option>
        <option value="1000X800">1000X800</option>
        <option value="1020X850">1020X850</option>
      </select>
      <select id="transicion" name="transicion">
        <option value="1">Estilo 1</option>
        <option value="2">Estilo 2</option>
        <option value="3">Estilo 3</option>
        <option value="4">Estilo 4</option>
        <option value="5">Estilo 5</option>

      </select>
      <label >Tipo de color</label>
      <select onclick="deshabilitarColor2()" id="tipoDeColor">
        <option >Color Plano</option>
        <option >Transicion</option>
      </select>
      <label>Color1:</label>
      <input id="color1" name="color1" type="color" value="#ff0000">
      <label>Color2:</label>
      <input id="color2" name="color2" type="color" value="#ff0000" disabled>
      <label >Tipo de borde</label>
      <select id="tipoDeBorde">
        <option value="liso">liso</option>
        <option value="grueso">grueso</option>
      </select>


      <input class="button" type="submit" value="Agregar Lamina" style="margin-top: 20px;">
      <div id="loginspinner" class="loader">Loading...</div>
      <hr>
    </form>
  </div>
  <h1>Que contiene la lamina</h1>
  <textarea id="mytextarea"></textarea>
  <a href="/" class="button">Back</a>
  <script>
    myStorage = window.localStorage;
    const urlParams = new URLSearchParams(window.location.search);
    document.getElementById("idPresentacion").value = urlParams.get('idPresentacion');
  </script>


</body>

</html>


