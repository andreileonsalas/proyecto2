<?php
// Initialize the session
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Presentaciones</title>
    <link rel="stylesheet" href="styles/styles.css">
    
    <script type="text/javascript" src="js/llamadasBasesDeDatos.js"></script>

</head>

<body>

    <div id="BuscarPresentacion">
        <form class="BuscarForm" action="javascript:buscarPresentacion()" method="POST">
            <hr>
            <label for="id_user">ID del usuario</label>
            <input id="id_user" name="id_user" type="text" required>
            <input class="button" type="submit" value="Buscar Presentacion">
            <hr>  
        </form>
    </div>

    <div id="Presentacion">
    </div>

</body>

</html>