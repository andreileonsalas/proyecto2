select * from laminas;
select * from archivos;
select * from laminas_archivos;
select * from referencias;
select * from presentaciones;
select * from presentaciones_laminas;
select * from usuarios;

insert into presentaciones (nombre, id_usuario, publico)
values ('El bicho siu', 10, true), ('Messi chiquito', 10, false);
-- 89 id de siuu -true
-- 90 id de messi chiquito --false
-- delete from presentaciones where id_usuario = 7


insert into laminas (id, nombre, detalles) VALUES (200, 'Intro', '{"tamano":"800x600","contenido": "<p><strong>CR7 el bicho</strong></p>CR7 el bichoBalon de oro en 2008, Mejor Jugador del Mundo ese ano segun la FIFA, máximo goleador en la Liga Inglesa, ganador de la Champions League. Antes de convertirse en el futbolista mas caro del mundo, Ronaldo acumulo titulos hasta llegar a ser esa figura unica, a mitad de camino entre el deporte y el espectaculo."}');
insert into presentaciones_laminas (id_presentacion, id_lamina, orden) values (89, 200, 1);

insert into laminas (id, nombre, detalles) VALUES (201, 'Desarrollo', '{"tamano":"800x600","contenido": "<p>El sabe perfectamente que, hoy en dia, la influencia de una estrella debe ir mas alla del terreno de juego. No basta con <strong>meter goles o ganar titulos</strong>. Hay que ser un modelo a seguir. <em>Hay que tener una imagen.</em></p>"}');
insert into presentaciones_laminas (id_presentacion, id_lamina, orden) values (89, 201, 3);

insert into laminas (id, nombre, detalles) VALUES (202, 'Conclusion', '{"tamano":"800x600","contenido": "<p>Cristiano Ronaldo dos Santos Aveiro (5 February 1985) is a Portugese football player who plays for Juventus and the Portugal national team. Ronaldo is a forward.</p>"}');
insert into presentaciones_laminas (id_presentacion, id_lamina, orden) values (89, 202, 2);
-- delete from laminas where id=200